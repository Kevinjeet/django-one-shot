from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.shortcuts import render, get_object_or_404

# Create your views here.


def todo_list_list(request):
    list = TodoList.objects.all()
    context = {"list": list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)

    context = {"list": list}

    return render(request, "todos/detail.html", context)
